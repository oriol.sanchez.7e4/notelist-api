package com.example.notelistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteListApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteListApiApplication.class, args);
    }

}
